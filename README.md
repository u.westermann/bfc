# bfc

Convert files that contain an array of binary values to text.

Needs Python 3 and the Python click library (see requirements.txt).


    Usage: bfc.py [OPTIONS] FILENAME

    Options:
      -e, --endianess [little|big]  endianess. default: 'little'.
      -u, --unsigned                assume binary input values are unsigned.
      -n, --num INTEGER             number of byes per input value. default: 4.
      -s, --separator TEXT          separator between output values. default: ', '
      -p, --prefix TEXT             prefix string. default: '['
      -o, --postfix TEXT            postfix string. default: ']'
      --help                        Show this message and exit.
