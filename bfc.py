#!/usr/bin/env python3

import click
import sys


@click.command()
@click.option("-e", "--endianess", type=click.Choice(["little", "big"]), default="little", help="endianess. default: 'little'.")
@click.option("-u", "--unsigned", is_flag=True, help="assume binary input values are unsigned.")
@click.option("-n", "--num", default=4, help="number of byes per input value. default: 4.", type=int)
@click.option("-s", "--separator", default=", ", help="separator between output values. default: ', '")
@click.option("-p", "--prefix", default="[", help="prefix string. default: '['")
@click.option("-o", "--postfix", default="]", help="postfix string. default: ']'")
@click.argument("filename")
def convert(endianess, unsigned, num, separator, prefix, postfix, filename):
    with open(filename, "rb") as bfile:
        bdata = bfile.read()

        if len(bdata) % num != 0:
            sys.stderr.write(f"warning. file length ({len(bdata)}) is not a multiple of num argument ({num})!\n")

        print(prefix, end='')

        while len(bdata) > 0:
            value = int.from_bytes(bdata[:num], byteorder=endianess, signed=not unsigned)

            bdata = bdata[num:]

            if len(bdata) > 0:
                print(f"{value}{separator}", end='')
            else:
                print(f"{value}", end='')

        print(postfix, end='')


if __name__ == '__main__':
    convert()
